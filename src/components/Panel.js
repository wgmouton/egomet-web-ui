import React, { Component } from 'react'

class Panel extends Component {
  render() {
    return (
      <article>
        <header>Header</header>
        <section>body</section>
      </article>
    );
  }
}

export default Panel;