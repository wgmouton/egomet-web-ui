import React, { Component } from 'react';

class Header extends Component {
  render() {
    return (
      <div>
        <h1>Willem Mouton</h1>
        <h2>Full Stack Developer</h2>
      </div>
    );
  }
}

export default Header;
