import React, { Component } from 'react'

class SidebarPoint extends Component {
  state = {
    title: this.props.title,
    subText: this.props.subText
  };

  render() {
    return (
      <section>
        <h3>{this.state.title}</h3>
        <ul>
          {
            this.state.subText.map(val =>
              <li>{val}</li>
            )
          }
        </ul>
      </section>
    );
  }
}

export default SidebarPoint;