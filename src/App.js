import React, { Component } from 'react';
import Header from './components/Header'
import SidebarPoint from './components/SidebarPoint'
import SocialLink from './components/SocialLink'
import Panel from './components/Panel'
import Summary from './Summary'
import Info from './Info'

/*
<div className="App">
        <canvas className="node-garden"></canvas>

        <Header/>

        <section>
          <SocialLink />
          <SocialLink />
          <SocialLink />
        </section>




        <aside>
          <SidebarPoint title="Tech" subText={["Scala", "Js"]}/>


          </aside>

          <Panel/>
          <Panel/>
          <Panel/>
          <Panel/>
  
  
  
  
  
          <h1>WG Mouton Portfolio</h1>
          <h2>As you can see its still a work in progress.</h2>
          <h3>Languages</h3>
          <ul>
            <li>Scala: * * * * *</li>
            <li>Javascript * * * *</li>
            <li>PHP: * * * *</li>
            <li>GoLang: * * *</li>
            <li>Java: * * *</li>
            <li>Python: * *</li>
            <li>Kotlin: *</li>
          </ul>
          <h3>Libraries / Frameworks</h3>
          <ul>
            <li>Play Framework (Scala) * * * * *</li>
            <li>Akka Http * * * *</li>
            <li>Akka * * *</li>
            <li>React * *</li>
          </ul>
          <h3>Stack Tools</h3>
          <ul>
            <li>Kubernates</li>
            <li>Docker</li>
            <li>Redis</li>
            <li>Mongo</li>
            <li>MySql</li>
            <li>Gitlab CI</li>
            <li>Travis CI</li>
            <li>Jenkins</li>
            <li>Netlify</li>
          </ul>
          <a href="https://goo.gl/forms/Wb1MtieNJaE5r3ZR2">Contact form.</a>
        </div>
*/

class App extends Component {

  changePage = (page) => {
    this.setState({ page })
  };

  state = {
    page: (<Summary updatePage={this.changePage}/>)
  };

  render() {
    return (
      <React.Fragment>
        {this.state.page}
      </React.Fragment>
    );
  }
}

export default App;
